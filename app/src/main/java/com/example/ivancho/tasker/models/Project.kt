package com.example.ivancho.tasker.models

import java.time.LocalDate

data class Project (val id: Long,
                    var name: String,
                    val dateCreated: LocalDate,
                    var description: String,
                    var isComplete: Boolean,
                    var dueDate: LocalDate,
                    var tasks: List<Task>)
