package com.example.ivancho.tasker.models

data class Tag(val id: Int,
		var tagName: String)
