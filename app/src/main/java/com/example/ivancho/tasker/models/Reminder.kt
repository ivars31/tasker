package com.example.ivancho.tasker.models

import java.util.*

data class Reminder(var id: Int,
                    var taskId: Int,
                    var dateTime: Date,
                    var description: String)
