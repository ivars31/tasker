package com.example.ivancho.tasker.database.dataSources

import android.content.Context
import com.example.ivancho.tasker.database.contracts.DBContract
import com.example.ivancho.tasker.models.Task

class TaskDataSource(context: Context) : DataSource(context) {
	fun createTask(task: Task): Task {
		val insertedRowId = database.insert(DBContract.Task.TABLE_NAME, null, task.toValues())
		task.id = insertedRowId
		return task
	}

	fun fetchTask(id: Long): Task {
		database.query(
				DBContract.Task.TABLE_NAME,
				DBContract.Task.ALL_COLUMNS,
				"id = ?",
				arrayOf(id.toString()),
				null,
				null,
				null
		)
		TODO()
	}
}
