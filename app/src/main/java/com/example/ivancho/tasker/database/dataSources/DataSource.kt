package com.example.ivancho.tasker.database.dataSources

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.ivancho.tasker.database.DBHelper

abstract class DataSource(val context: Context) {

	private val dbHelper: SQLiteOpenHelper
	internal var database: SQLiteDatabase

	init {
		dbHelper = DBHelper(context)
		database = dbHelper.readableDatabase
	}

	fun open() {
		database = dbHelper.writableDatabase
	}

	fun close() {
		database.close()
	}
}
