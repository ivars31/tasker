package com.example.ivancho.tasker.models

import android.content.ContentValues
import com.example.ivancho.tasker.database.contracts.DBContract
import java.time.LocalDate

data class Task(var id: Long,
                var name: String,
                var description: String,
                var dueDate: LocalDate,
                var isComplete: Boolean,
                var tags: List<Tag>,
                val creationDate: LocalDate) : PersistableActivity {

	override fun toValues(): ContentValues {
		val values = ContentValues()
		values.put(DBContract.Task.COLUMN_NAME, name)
		values.put(DBContract.Task.COLUMN_DESCRIPTION, description)
		values.put(DBContract.Task.COLUMN_IS_COMPLETE, isComplete)

		//TODO manage dates
//		values.put(DBContract.Task.COLUMN_CREATION_DATE, creationDate.toEpochDay())
//		values.put(DBContract.Task.COLUMN_CREATION_DATE, creationDate)

		return values

	}
}
