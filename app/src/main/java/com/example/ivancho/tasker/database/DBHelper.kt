package com.example.ivancho.tasker.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.ivancho.tasker.database.contracts.DBContract

class DBHelper(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
	companion object {
		const val DATABASE_VERSION = 1
		const val DATABASE_NAME = "Tasker.db"
	}

	override fun onCreate(db: SQLiteDatabase?) {
		db?.execSQL(DBContract.Task.SQL_CREATE)
	}

	override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
		db?.execSQL(DBContract.Task.SQL_DELETE)
		onCreate(db)
	}
}
