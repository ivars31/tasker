package com.example.ivancho.tasker.models

import android.content.ContentValues

interface PersistableActivity {
	fun toValues(): ContentValues
}
