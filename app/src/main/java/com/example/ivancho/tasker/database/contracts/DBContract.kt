package com.example.ivancho.tasker.database.contracts

import android.provider.BaseColumns

object DBContract{

	object Task : BaseColumns{
		const val ID = "id"
		const val TABLE_NAME = "Tasks"
		const val COLUMN_CREATION_DATE = "creationDate"
		const val COLUMN_NAME = "name"
		const val COLUMN_DESCRIPTION = "description"
		const val COLUMN_DUE_DATE = "dueDate"
		const val COLUMN_IS_COMPLETE = "isComplete"

		const val SQL_CREATE = "CREATE TABLE ${Task.TABLE_NAME}(" +
				"${Task.ID} INTEGER PRIMARY KEY, " +
				"${Task.COLUMN_NAME} TEXT, " +
				"${Task.COLUMN_DESCRIPTION} TEXT, " +
				"${Task.COLUMN_DUE_DATE} TEXT, " +
				"${Task.COLUMN_IS_COMPLETE} NUMERIC, " +
				"${Task.COLUMN_CREATION_DATE} NUMERIC)"

		const val SQL_DELETE = "DROP TABLE IF EXISTS ${Task.TABLE_NAME}"

		val ALL_COLUMNS = arrayOf(COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_IS_COMPLETE)
	}
}
